/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('CategorySymmetry', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		categoryId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Tag',
				key: 'id'
			},
			unique: true
		},
		symmetry: {
			type: DataTypes.ENUM('HALF','QUARTER'),
			allowNull: false,
			defaultValue: 'HALF'
		}
	}, {
		tableName: 'CategorySymmetry'
	});
};
