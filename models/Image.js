/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Image', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		image: {
			type: "MEDIUMBLOB",
			allowNull: false
		},
		contentType: {
			type: DataTypes.STRING(255),
			allowNull: false
		}
	}, {
		tableName: 'Image'
	});
};
