/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('C3P0TestTable', {
		a: {
			type: DataTypes.CHAR(1),
			allowNull: true
		}
	}, {
		tableName: 'C3P0TestTable'
	});
};
