/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ResellerEventDate', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Event',
				key: 'id'
			}
		},
		date: {
			type: DataTypes.DATEONLY,
			allowNull: false
		},
		soldTickets: {
			type: DataTypes.INTEGER(11),
			allowNull: false
		},
		allTickets: {
			type: DataTypes.INTEGER(11),
			allowNull: false
		},
		revenue: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		potentialRevenue: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		roi: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		marketShareSales: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		marketSales: {
			type: DataTypes.INTEGER(11),
			allowNull: false
		},
		marketShareListings: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		marketListings: {
			type: DataTypes.INTEGER(11),
			allowNull: false
		},
		pctShared: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		sellThrough: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		lastPriceChange: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		tableName: 'ResellerEventDate'
	});
};
