/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ListingPriceChangeSetObserver', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		listingPriceChangeSetId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'ListingPriceChangeSet',
				key: 'id'
			}
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'User',
				key: 'id'
			}
		},
		notificationTime: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		tableName: 'ListingPriceChangeSetObserver'
	});
};
