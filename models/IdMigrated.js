/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('IdMigrated', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		migrationName: {
			type: DataTypes.STRING(127),
			allowNull: false,
			defaultValue: 'ListingHistoryDuplicateRemoval'
		}
	}, {
		tableName: 'IdMigrated'
	});
};
