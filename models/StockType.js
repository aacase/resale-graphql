/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('StockType', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false
		}
	}, {
		tableName: 'StockType'
	});
};
