/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('VenueConfigurationTag', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		venueConfigurationId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'VenueConfiguration',
				key: 'id'
			}
		},
		tagId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Tag',
				key: 'id'
			}
		}
	}, {
		tableName: 'VenueConfigurationTag'
	});
};
