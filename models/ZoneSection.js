/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ZoneSection', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		venueConfigurationId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'VenueConfiguration',
				key: 'id'
			}
		},
		zoneName: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		sectionName: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		shape: {
			type: "MULTIPOLYGON",
			allowNull: false
		}
	}, {
		tableName: 'ZoneSection'
	});
};
