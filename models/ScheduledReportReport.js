/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ScheduledReportReport', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		scheduledReportId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'ScheduledReport',
				key: 'id'
			}
		},
		reportId: {
			type: DataTypes.INTEGER(6),
			allowNull: false,
			references: {
				model: 'Report',
				key: 'id'
			}
		}
	}, {
		tableName: 'ScheduledReportReport'
	});
};
