/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ForeignListingId', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		listingId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Listing',
				key: 'id'
			}
		},
		source: {
			type: DataTypes.ENUM('StubHub','EI','TN','TN_HOLD','TT','Qcue','QcueTM','SkyBox','TE','TU'),
			allowNull: false
		},
		foreignId: {
			type: DataTypes.STRING(255),
			allowNull: false
		}
	}, {
		tableName: 'ForeignListingId'
	});
};
