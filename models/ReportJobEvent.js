/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ReportJobEvent', {
		jobId: {
			type: DataTypes.STRING(36),
			allowNull: false
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Event',
				key: 'id'
			}
		}
	}, {
		tableName: 'ReportJobEvent'
	});
};
