/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('EventRefreshHistory', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Event',
				key: 'id'
			}
		},
		refreshType: {
			type: DataTypes.ENUM('FULL','PRIORITY','MANUAL','HISTORICAL'),
			allowNull: false,
			defaultValue: 'HISTORICAL'
		},
		date: {
			type: DataTypes.DATE,
			allowNull: false
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'User',
				key: 'id'
			}
		}
	}, {
		tableName: 'EventRefreshHistory'
	});
};
