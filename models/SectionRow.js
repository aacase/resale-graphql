/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('SectionRow', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		sectionId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Section',
				key: 'id'
			}
		},
		row: {
			type: DataTypes.INTEGER(3),
			allowNull: false
		},
		shape: {
			type: "MULTIPOLYGON",
			allowNull: false
		},
		startAngle: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		endAngle: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		distanceToCenter: {
			type: DataTypes.INTEGER(6),
			allowNull: true
		},
		angle: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		centerPoint: {
			type: "POINT",
			allowNull: true
		}
	}, {
		tableName: 'SectionRow'
	});
};
