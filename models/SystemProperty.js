/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('SystemProperty', {
		property: {
			type: DataTypes.STRING(255),
			allowNull: false,
			primaryKey: true
		},
		value: {
			type: DataTypes.STRING(255),
			allowNull: false
		}
	}, {
		tableName: 'SystemProperty'
	});
};
