/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Attachment', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		emailId: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		attachmentIndex: {
			type: DataTypes.INTEGER(11),
			allowNull: false
		},
		fileName: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		content: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		contentType: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		encoding: {
			type: DataTypes.STRING(255),
			allowNull: true
		}
	}, {
		tableName: 'Attachment'
	});
};
