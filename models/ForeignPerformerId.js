/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ForeignPerformerId', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		performerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Performer',
				key: 'id'
			}
		},
		source: {
			type: DataTypes.ENUM('StubHub','EI','SkyBox','TN','TT','MLB','SeatGeek','TE','TU'),
			allowNull: false
		},
		foreignId: {
			type: DataTypes.STRING(255),
			allowNull: false
		}
	}, {
		tableName: 'ForeignPerformerId'
	});
};
