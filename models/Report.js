/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Report', {
		id: {
			type: DataTypes.INTEGER(6),
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		nameKey: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		descriptionKey: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		reportLocation: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		adminOnly: {
			type: DataTypes.INTEGER(1),
			allowNull: true,
			defaultValue: '0'
		},
		permission: {
			type: DataTypes.ENUM('ADMIN','BROKER_DATA_SHARE','DASHBOARD','DATA_AND_REPORTING','FULL_PRICE','LIGHT_PRICE','MANAGE_USERS','SETUP'),
			allowNull: true
		},
		filterable: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '1'
		}
	}, {
		tableName: 'Report'
	});
};
