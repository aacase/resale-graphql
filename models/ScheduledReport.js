/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ScheduledReport', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'ScheduledAction',
				key: 'id'
			}
		},
		fromDate: {
			type: DataTypes.DATEONLY,
			allowNull: true
		},
		toDate: {
			type: DataTypes.DATEONLY,
			allowNull: true
		}
	}, {
		tableName: 'ScheduledReport'
	});
};
