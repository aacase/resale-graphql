/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ScheduledPriceChangeGroupTagType', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		scheduledPriceChangeGroupId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'ScheduledPriceChangeGroup',
				key: 'id'
			}
		},
		tagTypeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'TagType',
				key: 'id'
			}
		},
		ordering: {
			type: DataTypes.INTEGER(11),
			allowNull: false
		}
	}, {
		tableName: 'ScheduledPriceChangeGroupTagType'
	});
};
