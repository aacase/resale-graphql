/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ListingSeat', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		listingId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Listing',
				key: 'id'
			}
		},
		row: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		seat: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		quantity: {
			type: DataTypes.INTEGER(11),
			allowNull: false
		},
		manifestId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'Manifest',
				key: 'id'
			}
		},
		posStatus: {
			type: DataTypes.ENUM('AVAILABLE','SOLD','CANCELED'),
			allowNull: true,
			defaultValue: 'AVAILABLE'
		},
		posSalePrice: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		posSaleDate: {
			type: DataTypes.DATE,
			allowNull: true
		},
		stubHubStatus: {
			type: DataTypes.ENUM('AVAILABLE','SOLD','IGNORED'),
			allowNull: true,
			defaultValue: 'AVAILABLE'
		},
		stubHubSalePrice: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		stubHubSaleDate: {
			type: DataTypes.DATE,
			allowNull: true
		},
		mappedSaleExchange: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		rawSaleExchange: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		barcode: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		additionalProps: {
			type: DataTypes.STRING(2047),
			allowNull: true
		}
	}, {
		tableName: 'ListingSeat'
	});
};
