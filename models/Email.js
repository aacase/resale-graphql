/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Email', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		fromAddress: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		toAddress: {
			type: DataTypes.STRING(1024),
			allowNull: false
		},
		subject: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		content: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		mimeType: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		sent: {
			type: DataTypes.DATE,
			allowNull: false
		}
	}, {
		tableName: 'Email'
	});
};
