/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('UserGridColumn', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'User',
				key: 'id'
			}
		},
		grid: {
			type: DataTypes.ENUM('ZONES','DATA','VENUES','VENUECONFIGURATIONS','EVENTS','RECENT_ORDERS','GLOBAL_EVENTS'),
			allowNull: true
		},
		name: {
			type: DataTypes.STRING(63),
			allowNull: false
		},
		shown: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '1'
		},
		width: {
			type: DataTypes.INTEGER(6),
			allowNull: true
		}
	}, {
		tableName: 'UserGridColumn'
	});
};
