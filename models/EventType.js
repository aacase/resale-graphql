/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('EventType', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false,
			unique: true
		},
		tagId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Tag',
				key: 'id'
			}
		},
		eventDescription: {
			type: DataTypes.ENUM('TITLE','COMPETITORS','TITLE_COMPETITORS'),
			allowNull: false
		},
		startDate: {
			type: DataTypes.DATEONLY,
			allowNull: false
		},
		endDate: {
			type: DataTypes.DATEONLY,
			allowNull: false
		}
	}, {
		tableName: 'EventType'
	});
};
