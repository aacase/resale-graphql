/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Reseller', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false,
			unique: true
		},
		nickname: {
			type: DataTypes.STRING(10),
			allowNull: true
		},
		logoId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'Image',
				key: 'id'
			}
		},
		visible: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '1'
		},
		showSeats: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		timezone: {
			type: DataTypes.STRING(255),
			allowNull: true,
			defaultValue: 'America/New_York'
		},
		reviewPeriod: {
			type: DataTypes.INTEGER(6),
			allowNull: false,
			defaultValue: '3'
		},
		lastPosListingsRefresh: {
			type: DataTypes.DATE,
			allowNull: true
		},
		lastPosListingsMarketRefresh: {
			type: DataTypes.DATE,
			allowNull: true
		},
		stubHubUserGuid: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		stubHubUserToken: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		stubHubRefreshToken: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		stubHubConsumerKey: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		stubHubConsumerSecret: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		lastShared: {
			type: DataTypes.DATE,
			allowNull: true
		},
		pricingId: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		zonesPageAdminOnly: {
			type: DataTypes.INTEGER(1),
			allowNull: true,
			defaultValue: '0'
		},
		shareChannels: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		parentId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		reviewAction: {
			type: DataTypes.ENUM('SUBMIT','REJECT'),
			allowNull: false,
			defaultValue: 'REJECT'
		},
		beta: {
			type: DataTypes.INTEGER(1),
			allowNull: true,
			defaultValue: '0'
		},
		autoEnableEvents: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '1'
		},
		defaultPointOfSaleAccountId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'PointOfSaleCredential',
				key: 'id'
			}
		}
	}, {
		tableName: 'Reseller'
	});
};
