/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ListingPriceHistory', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		listingId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Listing',
				key: 'id'
			}
		},
		date: {
			type: DataTypes.DATE,
			allowNull: false
		},
		price: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		type: {
			type: DataTypes.ENUM('UNSUBMITTED','UNCONFIRMED','FACE_VALUE','QCUE_SUGGESTED'),
			allowNull: false
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'User',
				key: 'id'
			}
		}
	}, {
		tableName: 'ListingPriceHistory'
	});
};
