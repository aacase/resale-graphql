/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Sample_ListingPriceHistory', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		listingId: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		date: {
			type: DataTypes.DATE,
			allowNull: false
		},
		price: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		type: {
			type: DataTypes.ENUM('UNSUBMITTED','UNCONFIRMED','FACE_VALUE','QCUE_SUGGESTED'),
			allowNull: false
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: true
		}
	}, {
		tableName: 'Sample_ListingPriceHistory'
	});
};
