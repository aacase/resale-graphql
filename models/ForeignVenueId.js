/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ForeignVenueId', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		venueId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Venue',
				key: 'id'
			}
		},
		source: {
			type: DataTypes.ENUM('StubHub','EI','SkyBox','TN','TT','SeatGeek','TE','TU'),
			allowNull: false
		},
		foreignId: {
			type: DataTypes.STRING(255),
			allowNull: false
		}
	}, {
		tableName: 'ForeignVenueId'
	});
};
