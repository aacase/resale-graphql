/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ListingOutlierBackup', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		listingId: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false
		}
	}, {
		tableName: 'ListingOutlierBackup'
	});
};
