/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ListingPayment', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		listingId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Listing',
				key: 'id'
			}
		},
		referenceNumber: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		paymentAmount: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		paymentDate: {
			type: DataTypes.DATE,
			allowNull: false
		},
		lastUpdatedDate: {
			type: DataTypes.DATE,
			allowNull: false
		},
		lastUpdatedUserId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'User',
				key: 'id'
			}
		}
	}, {
		tableName: 'ListingPayment'
	});
};
