/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ForeignEventId', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Event',
				key: 'id'
			}
		},
		source: {
			type: DataTypes.ENUM('StubHub','EI','EI_LOCAL','TN','TT','Qcue','QcueTM','QcuePricing','MLB','SeatGeek','SkyBox','TE','TU'),
			allowNull: false
		},
		foreignId: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		manuallyReconciled: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		posName: {
			type: DataTypes.STRING(255),
			allowNull: true
		}
	}, {
		tableName: 'ForeignEventId'
	});
};
