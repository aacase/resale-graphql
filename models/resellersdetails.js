/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('resellersdetails', {
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '0'
		},
		resellerName: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		numFutureEvents: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '0'
		}
	}, {
		tableName: 'resellersdetails'
	});
};
