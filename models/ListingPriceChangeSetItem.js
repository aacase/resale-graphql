/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ListingPriceChangeSetItem', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		listingPriceChangeSetId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'ListingPriceChangeSet',
				key: 'id'
			}
		},
		listingId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Listing',
				key: 'id'
			}
		},
		proposerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'User',
				key: 'id'
			}
		},
		currentPrice: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		recommendedPrice: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		proposedPrice: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		newPrice: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		result: {
			type: DataTypes.STRING(511),
			allowNull: true
		},
		calculator: {
			type: DataTypes.STRING(255),
			allowNull: true
		}
	}, {
		tableName: 'ListingPriceChangeSetItem'
	});
};
