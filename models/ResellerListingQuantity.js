/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ResellerListingQuantity', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		timeRecorded: {
			type: DataTypes.DATE,
			allowNull: false
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		source: {
			type: DataTypes.ENUM('StubHub','EI','SkyBox','TN','TN_HOLD','TT','Qcue','QcueTM','TE','TU'),
			allowNull: true
		},
		quantity: {
			type: DataTypes.BIGINT,
			allowNull: false
		}
	}, {
		tableName: 'ResellerListingQuantity'
	});
};
