/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ResellerPointOfSaleAccount', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		pointOfSaleAccountId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'PointOfSaleCredential',
				key: 'id'
			}
		}
	}, {
		tableName: 'ResellerPointOfSaleAccount'
	});
};
