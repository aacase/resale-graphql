/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ResellerEventDeliveryTypeOverride', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerEventId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'ResellerEvent',
				key: 'id'
			}
		},
		exchangeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Exchange',
				key: 'id'
			}
		},
		deliveryTypeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'DeliveryType',
				key: 'id'
			}
		}
	}, {
		tableName: 'ResellerEventDeliveryTypeOverride'
	});
};
