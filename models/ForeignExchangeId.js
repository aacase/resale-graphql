/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ForeignExchangeId', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		exchangeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Exchange',
				key: 'id'
			}
		},
		source: {
			type: DataTypes.ENUM('EI','TN','TT','Qcue','SkyBox','TE','TU'),
			allowNull: false
		},
		foreignId: {
			type: DataTypes.STRING(255),
			allowNull: false
		}
	}, {
		tableName: 'ForeignExchangeId'
	});
};
