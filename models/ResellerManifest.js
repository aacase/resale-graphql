/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ResellerManifest', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		manifestId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Manifest',
				key: 'id'
			}
		},
		zone: {
			type: DataTypes.STRING(255),
			allowNull: false
		}
	}, {
		tableName: 'ResellerManifest'
	});
};
