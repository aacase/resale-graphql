/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Sample_Listing', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		firstSeen: {
			type: DataTypes.DATE,
			allowNull: false
		},
		lastSeen: {
			type: DataTypes.DATE,
			allowNull: false
		},
		initialPrice: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		price: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		cost: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		initialQuantity: {
			type: DataTypes.INTEGER(11),
			allowNull: false
		},
		stubHubZoneId: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		zone: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		stubHubSectionId: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		section: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		mappedSection: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		mappable: {
			type: DataTypes.INTEGER(1),
			allowNull: false
		},
		faceValue: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		deliveryMethod: {
			type: DataTypes.STRING(20),
			allowNull: false,
			defaultValue: ''
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		area: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		newPrice: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		newPriceCreationTime: {
			type: DataTypes.DATE,
			allowNull: true
		},
		newPriceType: {
			type: DataTypes.ENUM('UNSUBMITTED','UNCONFIRMED','POS_CHANGE'),
			allowNull: true
		},
		recommendedPrice: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		newPriceUserId: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		sellerPrice: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		buyerPrice: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		primarySource: {
			type: DataTypes.ENUM('StubHub','EI','TT','Qcue','QcueTM','TN','SkyBox','TE','TU'),
			allowNull: true
		},
		shareType: {
			type: DataTypes.ENUM('HELD','SHARED','UNSHARED'),
			allowNull: true,
			defaultValue: 'SHARED'
		},
		payoutPerTicket: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		linkedListingId: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		holdExpiration: {
			type: DataTypes.DATE,
			allowNull: true
		},
		markup: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			defaultValue: '0.000'
		},
		pointOfSaleCredentialId: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		splitVector: {
			type: DataTypes.STRING(1023),
			allowNull: true
		},
		lastSharedDate: {
			type: DataTypes.DATE,
			allowNull: true
		},
		lastSharedQuantity: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		}
	}, {
		tableName: 'Sample_Listing'
	});
};
