/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('exchangedetails', {
		exchangeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '0'
		},
		exchangeName: {
			type: DataTypes.STRING(255),
			allowNull: true
		}
	}, {
		tableName: 'exchangedetails'
	});
};
