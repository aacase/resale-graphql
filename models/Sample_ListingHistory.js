/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Sample_ListingHistory', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		listingId: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		date: {
			type: DataTypes.DATE,
			allowNull: false
		},
		price: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		quantity: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		oldPrice: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		oldQuantity: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		type: {
			type: DataTypes.ENUM('UNSUBMITTED','UNCONFIRMED','FACE_VALUE','QCUE_SUGGESTED','POS_CHANGE'),
			allowNull: false
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: true
		}
	}, {
		tableName: 'Sample_ListingHistory'
	});
};
