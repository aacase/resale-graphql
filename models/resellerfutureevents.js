/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('resellerfutureevents', {
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '0'
		},
		eventPosName: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		eventDate: {
			type: DataTypes.DATE,
			allowNull: false
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '0'
		},
		resellerName: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		unsoldListings: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '0'
		},
		unsoldSeats: {
			type: DataTypes.DECIMAL,
			allowNull: true
		}
	}, {
		tableName: 'resellerfutureevents'
	});
};
