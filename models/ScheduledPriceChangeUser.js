/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ScheduledPriceChangeUser', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		scheduledPriceChangeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'ScheduledPriceChange',
				key: 'id'
			}
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'User',
				key: 'id'
			}
		}
	}, {
		tableName: 'ScheduledPriceChangeUser'
	});
};
