/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('PointOfSaleCredential', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		accountId: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		token: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		secret: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		enabled: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '1'
		},
		pointOfSaleId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'PointOfSale',
				key: 'id'
			}
		},
		shared: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		timezone: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		markup: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		lastAccountingFileUploadDate: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		tableName: 'PointOfSaleCredential'
	});
};
