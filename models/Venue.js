/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Venue', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false,
			unique: true
		},
		venueInfoUrl: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		venueEventsUrl: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		address1: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		address2: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		city: {
			type: DataTypes.STRING(50),
			allowNull: true
		},
		state: {
			type: DataTypes.STRING(50),
			allowNull: true
		},
		country: {
			type: DataTypes.STRING(50),
			allowNull: true
		},
		zipCode: {
			type: DataTypes.STRING(20),
			allowNull: true
		},
		latitude: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		longitude: {
			type: DataTypes.FLOAT,
			allowNull: true
		},
		timezone: {
			type: DataTypes.STRING(255),
			allowNull: false,
			defaultValue: 'America/New_York'
		},
		collectListings: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		}
	}, {
		tableName: 'Venue'
	});
};
