/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('futureevents', {
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '0'
		},
		eventPosName: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		performerId: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		opponentId: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		eventDate: {
			type: DataTypes.DATE,
			allowNull: false
		},
		numResellers: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '0'
		},
		unsoldListings: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '0'
		},
		unsoldSeats: {
			type: DataTypes.DECIMAL,
			allowNull: true
		}
	}, {
		tableName: 'futureevents'
	});
};
