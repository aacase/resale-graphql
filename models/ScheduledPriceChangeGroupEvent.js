/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ScheduledPriceChangeGroupEvent', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		scheduledPriceChangeGroupId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'ScheduledPriceChangeGroup',
				key: 'id'
			}
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Event',
				key: 'id'
			}
		}
	}, {
		tableName: 'ScheduledPriceChangeGroupEvent'
	});
};
