/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('TagType', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false,
			unique: true
		},
		qcueTagType: {
			type: DataTypes.ENUM('CATEGORY','PERFORMER','OPPONENT','VENUE','EVENT_TYPE','EVENT_DESCR','MONTH','DAY','YEAR','PART_OF_WEEK','SCHEDULED','PROMOTIONS','OWNER','CALCULATOR','CLASSIFICATION','PERCENTILE','PRICING_OPTION','STRATEGY_NAME','ENABLEMENT_STATUS'),
			allowNull: true,
			unique: true
		},
		applicable: {
			type: DataTypes.ENUM('PERFORMER','VENUE','EVENT','ALL'),
			allowNull: true
		},
		eventHover: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		universal: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '1'
		},
		autoTagged: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		multiple: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		searchable: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '1'
		},
		clientSetup: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		}
	}, {
		tableName: 'TagType'
	});
};
