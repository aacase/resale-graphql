/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Event', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		venueConfigurationId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'VenueConfiguration',
				key: 'id'
			}
		},
		date: {
			type: DataTypes.DATE,
			allowNull: false
		},
		posName: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		description: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		eventInfoUrl: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		performerId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'Performer',
				key: 'id'
			}
		},
		opponentId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'Performer',
				key: 'id'
			}
		},
		lastRefresh: {
			type: DataTypes.DATE,
			allowNull: true
		},
		eventTypeId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'EventType',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		dateTBD: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		timeTBD: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		demo: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		temperature: {
			type: DataTypes.INTEGER(6),
			allowNull: true
		},
		probabilityOfPrecipitation: {
			type: DataTypes.INTEGER(6),
			allowNull: true
		},
		windSpeed: {
			type: DataTypes.INTEGER(6),
			allowNull: true
		},
		weatherConditions: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		weatherIcon: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		cancelled: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		temperatureLow: {
			type: DataTypes.INTEGER(6),
			allowNull: true
		},
		temperatureHigh: {
			type: DataTypes.INTEGER(6),
			allowNull: true
		},
		archived: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		enabled: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		selected: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '0'
		},
		shared: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '0'
		},
		isRefeshing: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '0'
		},
		isRefreshing: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '0'
		}
	}, {
		tableName: 'Event'
	});
};
