/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ListingDiagnosticTranslation', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		diagnosticTranslationKey: {
			type: DataTypes.STRING(255),
			allowNull: false,
			unique: true
		},
		diagnosticTranslation: {
			type: DataTypes.STRING(1023),
			allowNull: false
		},
		showType: {
			type: DataTypes.ENUM('ALWAYS','NEVER','ADMIN'),
			allowNull: false,
			defaultValue: 'ALWAYS'
		}
	}, {
		tableName: 'ListingDiagnosticTranslation'
	});
};
