/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Sample_ListingOutlier', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		listingId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			unique: true
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false
		}
	}, {
		tableName: 'Sample_ListingOutlier'
	});
};
