/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ScheduledAction', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		action: {
			type: DataTypes.ENUM('REPORTS','PRICE_CHANGES'),
			allowNull: false
		},
		cron: {
			type: DataTypes.STRING(50),
			allowNull: false
		},
		enabled: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		nextRunTime: {
			type: DataTypes.DATE,
			allowNull: true
		},
		startDate: {
			type: DataTypes.DATEONLY,
			allowNull: true
		},
		latestRunTime: {
			type: DataTypes.DATE,
			allowNull: true
		},
		frequency: {
			type: DataTypes.ENUM('DAILY','WEEKLY','DAYS_BEFORE_EVENT'),
			allowNull: false
		}
	}, {
		tableName: 'ScheduledAction'
	});
};
