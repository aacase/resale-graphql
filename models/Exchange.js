/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Exchange', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: true,
			unique: true
		}
	}, {
		tableName: 'Exchange'
	});
};
