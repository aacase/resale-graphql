/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('performers', {
		performerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '0'
		},
		peformerName: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		performerNickname: {
			type: DataTypes.STRING(128),
			allowNull: true
		},
		numFutureEventsPerformer: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		numTotalListingsPerformer: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		numFutureEventsOpponent: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		numTotalListingsOpponent: {
			type: DataTypes.BIGINT,
			allowNull: true
		}
	}, {
		tableName: 'performers'
	});
};
