/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ScheduledReportTagType', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		scheduledReportId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'ScheduledReport',
				key: 'id'
			}
		},
		tagTypeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'TagType',
				key: 'id'
			}
		},
		ordering: {
			type: DataTypes.INTEGER(11),
			allowNull: false
		}
	}, {
		tableName: 'ScheduledReportTagType'
	});
};
