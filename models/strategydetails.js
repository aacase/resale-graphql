/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('strategydetails', {
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		StrategyId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			defaultValue: '0'
		},
		TimeframeId: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		strategyName: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		timeFrame: {
			type: DataTypes.STRING(23),
			allowNull: true
		},
		maxPercentChange: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		reviewPeriod: {
			type: DataTypes.INTEGER(6),
			allowNull: true
		},
		rangeDaysMin: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		rangeDaysMax: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		reviewAction: {
			type: DataTypes.ENUM('SUBMIT','REJECT'),
			allowNull: false,
			defaultValue: 'REJECT'
		},
		priceDistributionType: {
			type: DataTypes.ENUM('SAME','PRICE_RELATIVE'),
			allowNull: true,
			defaultValue: 'SAME'
		},
		priceRelativeCustomValue: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			defaultValue: '1.00'
		},
		priceFloorType: {
			type: DataTypes.ENUM('NO_FLOOR','INDIVIDUAL_LISTING_COST','STRATEGY_FLOOR'),
			allowNull: false,
			defaultValue: 'STRATEGY_FLOOR'
		},
		strategyFloor: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			defaultValue: '6.00'
		},
		individualListingCostFloor: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			defaultValue: '0.00'
		},
		individualListingCostFloorPct: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		marketOffsetPriceValue: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			defaultValue: '0.0000'
		},
		marketOffsetPricePct: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		competitiveAreaType: {
			type: DataTypes.ENUM('ZONE','SECTION','CUSTOM','SURROUNDING_ROWS'),
			allowNull: false,
			defaultValue: 'ZONE'
		},
		surroundingRowsRange: {
			type: DataTypes.INTEGER(3),
			allowNull: false,
			defaultValue: '3'
		},
		marketBasePositionType: {
			type: DataTypes.ENUM('VALUE','PRICE'),
			allowNull: false,
			defaultValue: 'VALUE'
		},
		marketBasePercentile: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			defaultValue: '0.00'
		},
		surroundingSectionsRange: {
			type: DataTypes.INTEGER(6),
			allowNull: false,
			defaultValue: '1'
		},
		expandMarket: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '0'
		},
		expandMarketLimitType: {
			type: DataTypes.ENUM('LEVEL','ZONE','VENUE'),
			allowNull: false,
			defaultValue: 'LEVEL'
		},
		singleListingExclusion: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '0'
		},
		similarListingExclusion: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '0'
		},
		adjustSingles: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '0'
		},
		adjustSinglesPct: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '0'
		},
		adjustSinglesValue: {
			type: DataTypes.DECIMAL,
			allowNull: true,
			defaultValue: '0.00'
		},
		adjustPacks: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '0'
		},
		adjustPackMinQty: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		adjustPackType: {
			type: DataTypes.ENUM('UP','DOWN'),
			allowNull: false,
			defaultValue: 'UP'
		},
		adjustPackPct: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '0'
		},
		adjustPackValue: {
			type: DataTypes.DECIMAL,
			allowNull: true,
			defaultValue: '0.00'
		},
		priceRelativeType: {
			type: DataTypes.ENUM('SMART','CUSTOM'),
			allowNull: false,
			defaultValue: 'SMART'
		},
		priceRelativeCustomPct: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: '0'
		},
		numFutureEvents: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '0'
		},
		Automatic?: {
			type: DataTypes.INTEGER(1),
			allowNull: true,
			defaultValue: '0'
		}
	}, {
		tableName: 'strategydetails'
	});
};
