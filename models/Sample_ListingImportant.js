/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Sample_ListingImportant', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		listingId: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		importantListingId: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		importantListingType: {
			type: DataTypes.ENUM('EXCLUDED','EXPANDED','BASE_SET'),
			allowNull: false
		}
	}, {
		tableName: 'Sample_ListingImportant'
	});
};
