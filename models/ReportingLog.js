/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ReportingLog', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		jobId: {
			type: DataTypes.STRING(36),
			allowNull: false
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'User',
				key: 'id'
			}
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		reportId: {
			type: DataTypes.INTEGER(6),
			allowNull: false,
			references: {
				model: 'Report',
				key: 'id'
			}
		},
		startTime: {
			type: DataTypes.DATE,
			allowNull: false
		},
		startBuildTime: {
			type: DataTypes.DATE,
			allowNull: true
		},
		endBuildTime: {
			type: DataTypes.DATE,
			allowNull: true
		},
		status: {
			type: DataTypes.ENUM('COMPLETE','WORKING','RETRY','ERROR','TERMINATE'),
			allowNull: true
		},
		reportName: {
			type: DataTypes.STRING(256),
			allowNull: false,
			defaultValue: 'Obsolete Report'
		}
	}, {
		tableName: 'ReportingLog'
	});
};
