/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ParsedTicket', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'User',
				key: 'id'
			}
		},
		sourceFile: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		parsingStartDate: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'Event',
				key: 'id'
			}
		},
		section: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		row: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		seat: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		price: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		barcode: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		pdf: {
			type: "MEDIUMBLOB",
			allowNull: true
		},
		error: {
			type: DataTypes.STRING(1023),
			allowNull: true
		}
	}, {
		tableName: 'ParsedTicket'
	});
};
