/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('PricingOption', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		tagId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'Tag',
				key: 'id'
			}
		},
		pricingChannel: {
			type: DataTypes.ENUM('POS','StubHub'),
			allowNull: true
		},
		roundingAmount: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		roundingChannel: {
			type: DataTypes.ENUM('POS','StubHub'),
			allowNull: true
		},
		gaSeats: {
			type: DataTypes.INTEGER(1),
			allowNull: true
		},
		pricing: {
			type: DataTypes.INTEGER(1),
			allowNull: true
		},
		calculatorTagId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'Tag',
				key: 'id'
			}
		},
		type: {
			type: DataTypes.ENUM('UNIVERSAL','NO_CATEGORY','CATEGORY'),
			allowNull: true,
			defaultValue: 'CATEGORY'
		}
	}, {
		tableName: 'PricingOption'
	});
};
