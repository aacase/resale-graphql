/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('PerformerTag', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		performerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Performer',
				key: 'id'
			}
		},
		tagId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Tag',
				key: 'id'
			}
		}
	}, {
		tableName: 'PerformerTag'
	});
};
