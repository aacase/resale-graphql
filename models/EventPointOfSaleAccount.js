/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('EventPointOfSaleAccount', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Event',
				key: 'id'
			}
		},
		pointOfSaleAccountId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'PointOfSaleCredential',
				key: 'id'
			}
		},
		lastRefresh: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		tableName: 'EventPointOfSaleAccount'
	});
};
