/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ListingInvoice', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		externalOrderNumber: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		invoiceNumber: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		posSaleGrandTotal: {
			type: DataTypes.DECIMAL,
			allowNull: false
		}
	}, {
		tableName: 'ListingInvoice'
	});
};
