/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ListingPriceChangeApprover', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		listingPriceChangeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'ListingPriceChange',
				key: 'id'
			}
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'User',
				key: 'id'
			}
		},
		notificationTime: {
			type: DataTypes.DATE,
			allowNull: true
		},
		selected: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		}
	}, {
		tableName: 'ListingPriceChangeApprover'
	});
};
