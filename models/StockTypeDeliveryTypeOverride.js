/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('StockTypeDeliveryTypeOverride', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		exchangeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Exchange',
				key: 'id'
			}
		},
		stockTypeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'StockType',
				key: 'id'
			}
		},
		deliveryTypeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'DeliveryType',
				key: 'id'
			}
		}
	}, {
		tableName: 'StockTypeDeliveryTypeOverride'
	});
};
