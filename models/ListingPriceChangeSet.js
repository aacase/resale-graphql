/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ListingPriceChangeSet', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		submitterId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'User',
				key: 'id'
			}
		},
		submissionTime: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		tableName: 'ListingPriceChangeSet'
	});
};
