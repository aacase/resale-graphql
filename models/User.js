/*  */

module.exports = function (sequelize, DataTypes) {
	return sequelize.define('User', {
		name: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		emailAddress: {
			type: DataTypes.STRING(255),
			allowNull: false,
			unique: true
		},
		locale: {
			type: DataTypes.STRING(45),
			allowNull: true
		},
		lastLoginTime: {
			type: DataTypes.DATE,
			allowNull: true
		},
		roleId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			defaultValue: '4',
			references: {
				model: 'Role',
				key: 'id'
			}
		},
		beta: {
			type: DataTypes.INTEGER(4),
			allowNull: true,
			defaultValue: '0'
		}
	}, {
			tableName: 'User'
		});
};
