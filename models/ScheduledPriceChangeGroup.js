/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ScheduledPriceChangeGroup', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		deleted: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		}
	}, {
		tableName: 'ScheduledPriceChangeGroup'
	});
};
