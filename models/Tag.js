/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Tag', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		tagTypeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'TagType',
				key: 'id'
			}
		},
		ordering: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		}
	}, {
		tableName: 'Tag'
	});
};
