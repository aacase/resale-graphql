/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Performer', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false,
			unique: true
		},
		collectListings: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		nickname: {
			type: DataTypes.STRING(128),
			allowNull: true
		},
		venueConfigurationId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'VenueConfiguration',
				key: 'id'
			}
		}
	}, {
		tableName: 'Performer'
	});
};
