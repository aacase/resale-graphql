/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('StockTypeDeliveryType', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		stockTypeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'StockType',
				key: 'id'
			}
		},
		deliveryTypeId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'DeliveryType',
				key: 'id'
			}
		}
	}, {
		tableName: 'StockTypeDeliveryType'
	});
};
