/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Manifest', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		venueConfigurationId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'VenueConfiguration',
				key: 'id'
			}
		},
		priceLevelCode: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		section: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		sectionAliases: {
			type: DataTypes.STRING(2048),
			allowNull: false
		},
		row: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		rowAliases: {
			type: DataTypes.STRING(1024),
			allowNull: true
		},
		number: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		priceLevelName: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		polygonName: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		sectionId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'Section',
				key: 'id'
			}
		},
		sectionRowId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'SectionRow',
				key: 'id'
			}
		},
		region: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		level: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		rowsFromFront: {
			type: DataTypes.INTEGER(6),
			allowNull: true
		},
		pricingManifestId: {
			type: DataTypes.BIGINT,
			allowNull: true
		},
		pricingPriceLevelName: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		locationScore: {
			type: DataTypes.DECIMAL,
			allowNull: true
		}
	}, {
		tableName: 'Manifest'
	});
};
