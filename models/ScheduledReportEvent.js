/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ScheduledReportEvent', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		scheduledReportId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'ScheduledReport',
				key: 'id'
			}
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Event',
				key: 'id'
			}
		}
	}, {
		tableName: 'ScheduledReportEvent'
	});
};
