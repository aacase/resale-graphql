/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Sample_ListingDiagnostic', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		listingId: {
			type: DataTypes.BIGINT,
			allowNull: false
		},
		diagnosticKey: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		diagnosticValue: {
			type: DataTypes.STRING(1023),
			allowNull: false
		},
		diagnosticVars: {
			type: DataTypes.STRING(1023),
			allowNull: true
		}
	}, {
		tableName: 'Sample_ListingDiagnostic'
	});
};
