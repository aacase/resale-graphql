/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ResellerEvent', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Event',
				key: 'id'
			}
		},
		markup: {
			type: DataTypes.DECIMAL,
			allowNull: false,
			defaultValue: '0.000'
		},
		lastPriceChange: {
			type: DataTypes.DATE,
			allowNull: true
		},
		sellThrough: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		cost: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		revenue: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		quantity: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		percentUnshared: {
			type: DataTypes.DECIMAL,
			allowNull: true,
			defaultValue: '0.000'
		},
		recalculate: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		resellerStubHubTickets: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			defaultValue: '0'
		},
		stubHubTickets: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			defaultValue: '0'
		},
		profit: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		enabled: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		outlierTickets: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			defaultValue: '0'
		},
		soldTicketsLast24Hours: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			defaultValue: '0'
		},
		minPriceSoldTicketsLast24Hours: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		maxPriceSoldTicketsLast24Hours: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		unsharedZones: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			defaultValue: '0'
		},
		autoEnabled: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		stockTypeId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'StockType',
				key: 'id'
			}
		},
		deliveryTypeId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'DeliveryType',
				key: 'id'
			}
		},
		oldestPriceChange: {
			type: DataTypes.DATE,
			allowNull: true
		},
		potentialRevenue: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		marketShareSales: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		marketShareListings: {
			type: DataTypes.DECIMAL,
			allowNull: true
		}
	}, {
		tableName: 'ResellerEvent'
	});
};
