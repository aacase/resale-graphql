/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('EventTag', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		eventId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Event',
				key: 'id'
			}
		},
		tagId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Tag',
				key: 'id'
			}
		}
	}, {
		tableName: 'EventTag'
	});
};
