/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('EventPointOfSaleAccountReseller', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		eventPointOfSaleAccountId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'EventPointOfSaleAccount',
				key: 'id'
			}
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: true,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		}
	}, {
		tableName: 'EventPointOfSaleAccountReseller'
	});
};
