/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ResellerObserver', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		resellerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Reseller',
				key: 'id'
			}
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'User',
				key: 'id'
			}
		},
		priceUpdateNotification: {
			type: DataTypes.ENUM('AT_OCCURRENCE','HOURLY','EVERY_3_HOURS','DAILY'),
			allowNull: false,
			defaultValue: 'HOURLY'
		},
		priceUpdateNotificationTime: {
			type: DataTypes.STRING(50),
			allowNull: false,
			defaultValue: '0 0 12 * * *'
		}
	}, {
		tableName: 'ResellerObserver'
	});
};
