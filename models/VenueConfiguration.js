/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('VenueConfiguration', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		venueId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Venue',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		hasManifest: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		centerPoint: {
			type: "POINT",
			allowNull: true
		},
		centerRefPoint: {
			type: "POINT",
			allowNull: true
		},
		hasPolygonManifestErrors: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		},
		needToRemapListings: {
			type: DataTypes.DATE,
			allowNull: true
		},
		needToRemapZones: {
			type: DataTypes.DATE,
			allowNull: true
		},
		backgroundImageEnvelope: {
			type: "POLYGON",
			allowNull: true
		},
		pricingVenueConfigurationName: {
			type: DataTypes.STRING(511),
			allowNull: true
		},
		pricingVenueConfigurationId: {
			type: DataTypes.BIGINT,
			allowNull: true
		}
	}, {
		tableName: 'VenueConfiguration'
	});
};
