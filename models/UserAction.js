/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('UserAction', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'User',
				key: 'id'
			}
		},
		window: {
			type: DataTypes.ENUM('HOME'),
			allowNull: false
		},
		view: {
			type: DataTypes.STRING(63),
			allowNull: true
		},
		area: {
			type: DataTypes.ENUM('MAIN','NAVIGATION'),
			allowNull: false
		},
		action: {
			type: DataTypes.STRING(63),
			allowNull: false
		},
		actionDetails: {
			type: DataTypes.STRING(255),
			allowNull: true
		},
		date: {
			type: DataTypes.DATE,
			allowNull: false
		}
	}, {
		tableName: 'UserAction'
	});
};
