/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('PointOfSale', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false,
			unique: true
		},
		abbreviation: {
			type: DataTypes.STRING(63),
			allowNull: false,
			unique: true
		},
		source: {
			type: DataTypes.ENUM('EI','SkyBox','TN','TT','Qcue','QcueTM','TE','TU','QP'),
			allowNull: false
		},
		includesSold: {
			type: DataTypes.INTEGER(4),
			allowNull: false,
			defaultValue: '0'
		},
		priority: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		}
	}, {
		tableName: 'PointOfSale'
	});
};
