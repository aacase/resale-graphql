/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('RolePermission', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		roleId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'Role',
				key: 'id'
			}
		},
		permission: {
			type: DataTypes.ENUM('ADMIN','BROKER_DATA_SHARE','DASHBOARD','DATA_AND_REPORTING','FULL_PRICE','LIGHT_PRICE','MANAGE_USERS','SETUP'),
			allowNull: false
		}
	}, {
		tableName: 'RolePermission'
	});
};
