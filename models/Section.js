/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Section', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		venueConfigurationId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'VenueConfiguration',
				key: 'id'
			}
		},
		shape: {
			type: "MULTIPOLYGON",
			allowNull: false
		}
	}, {
		tableName: 'Section'
	});
};
