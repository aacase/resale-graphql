/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('UserProperty', {
		userId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'User',
				key: 'id'
			}
		},
		property: {
			type: DataTypes.STRING(255),
			allowNull: false
		},
		value: {
			type: DataTypes.STRING(2047),
			allowNull: false
		}
	}, {
		tableName: 'UserProperty'
	});
};
