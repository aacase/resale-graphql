/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ListingPriceChange', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'Listing',
				key: 'id'
			}
		},
		price: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		date: {
			type: DataTypes.DATE,
			allowNull: false
		},
		proposerId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'User',
				key: 'id'
			}
		},
		proposedPrice: {
			type: DataTypes.DECIMAL,
			allowNull: false
		},
		reviewPeriod: {
			type: DataTypes.INTEGER(6),
			allowNull: false,
			defaultValue: '3'
		},
		endOfReviewPeriodAction: {
			type: DataTypes.ENUM('SUBMIT','REJECT'),
			allowNull: false,
			defaultValue: 'REJECT'
		},
		error: {
			type: DataTypes.STRING(511),
			allowNull: true
		},
		proposerSelected: {
			type: DataTypes.INTEGER(1),
			allowNull: false,
			defaultValue: '0'
		}
	}, {
		tableName: 'ListingPriceChange'
	});
};
