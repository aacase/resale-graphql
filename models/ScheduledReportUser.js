/*  */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('ScheduledReportUser', {
		id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		scheduledReportId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'ScheduledReport',
				key: 'id'
			}
		},
		userId: {
			type: DataTypes.BIGINT,
			allowNull: false,
			references: {
				model: 'User',
				key: 'id'
			}
		}
	}, {
		tableName: 'ScheduledReportUser'
	});
};
