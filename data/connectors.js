import Sequelize from 'sequelize';
import SequelizeAuto from 'sequelize-auto';
import _ from 'lodash';
import fetch from 'node-fetch';

const db = new Sequelize('DBNAME', "USER", "PASS", {
  dialect: 'mysql', //postgres, mongodb, sqlite
  host:'localhost',
  define: {
    timestamps: false
}
});

db.authenticate()
.then(() => {
  console.log('Connection has been established successfully.');
})
.catch(err => {
  console.error('Unable to connect to the database:', err);
});

// const auto = new SequelizeAuto('database', 'user', 'pass');

// auto.run(function (err) {
//   if (err) throw err;
//   console.log(auto.tables); // table list
//   console.log(auto.foreignKeys); // foreign key list
// });

//REST
const FortuneCookie = {
  getOne() {
    return fetch('http://fortunecookieapi.herokuapp.com/v1/cookie')
      .then(res => res.json())
      .then(res => {
        return res[0].fortune.message;
      });
  },
}; 

//DB Models
const UserModel = db.import('../models/User'); 
const User = UserModel

const PointOfSaleCredentialModel = db.import('../models/PointOfSaleCredential')
const PointOfSaleCredential = PointOfSaleCredentialModel

export {User,FortuneCookie, PointOfSaleCredential};