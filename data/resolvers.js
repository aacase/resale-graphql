import { User, FortuneCookie, PointOfSaleCredential, db } from './connectors';
const resolvers = {
    Query: {
      user(_, args) {
        return User.findOne({ where: args });
      },
      allUsers(_, args) {
       if (args.name){
        return User.findAll({
          where:{
            name: {$like:"%"+args.name+'%'}
          },
          limit: 25
        });
       }
       return User.findAll()
      },
      pointOfSaleCredential(_, args) {
        return PointOfSaleCredential.findOne({ where: args });
      },
      allPointOfSaleCredentials(_, args) {
       if (args.accountId){
        return PointOfSaleCredential.findAll({
          where:{
            accountId: {$like:"%"+args.accountId+'%'}
          },
          limit: 25
        });
       }
       return PointOfSaleCredential.findAll()
      },
      getFortuneCookie() {
        return FortuneCookie.getOne();
      }
    },
    Mutation:{
      createPointOfSaleCredential(_, args){
        let record = args.input
        return PointOfSaleCredential.create(record)
      },
      updatePointOfSaleCredential(_, args){
        return PointOfSaleCredential.update(args.input, { where: { id: {$eq: args.id}}})
        .then(record => PointOfSaleCredential.findOne({ where: args.id }))
      },
     deletePointOfSaleCredential(_, args){
        return PointOfSaleCredential.destroy({ where: { id: {$eq: args.id }}})
      },
    }
  };
  
  export default resolvers;