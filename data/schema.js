import { makeExecutableSchema} from 'graphql-tools';
import resolvers from './resolvers';

const typeDefs = `
type Query {
  user(name: String, id: Int): User
  allUsers(name: String): [User]
  pointOfSaleCredential(accountId: String, id: Int): PointOfSaleCredential
  allPointOfSaleCredentials(accountId: String): [PointOfSaleCredential]
  getFortuneCookie: String # we'll use this later
  fake: Fake
},
type User{
  name: String
  id: Int,
  resellerId: Int,
  emailAddress: String,
  roleId: Int,
  beta: Boolean
},
input PointOfSaleCredentialInput{
  name: String
  resellerId: Int,
  accountId: String,
  token: String,
  secret: String,
  pointOfSaleId: Int,
  shared: Boolean
},
type PointOfSaleCredential{
  id: Int,
  name: String
  resellerId: Int,
  accountId: String,
  token: String,
  secret: String,
  pointOfSaleId: Int,
  shared: Boolean
},
type Fake{
  data: String
}
type Mutation {
  createPointOfSaleCredential(input: PointOfSaleCredentialInput): PointOfSaleCredential
  updatePointOfSaleCredential(id: Int, input: PointOfSaleCredentialInput): PointOfSaleCredential
  deletePointOfSaleCredential(id: Int): PointOfSaleCredential
}

`;

// Add resolvers option to this call
const schema = makeExecutableSchema({ typeDefs, resolvers });
export default schema;

